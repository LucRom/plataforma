using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy_Expander : MonoBehaviour
{
    [SerializeField] int phase = 0;
    [SerializeField] float expanderSize;
    [SerializeField] float phasetimer;
    [SerializeField] float timer = 0;
    void Start()
    {
        timer = phasetimer;
    }
    void Update()
    {
        switch (phase)
        {
            case 0:
                transform.localScale = new Vector3(1,1, 0.5f);
                Timer();
                break; 
            case 1:
                Timer();
                break;
            case 2:
                Timer();
                break;
            case 3:
                Timer();
                break;
            case 4:
                Timer();
                break;
            case 5:
                phase= 0;
                break;
            default: 
                break;
        }
    }
    private void Timer()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            ChangeScale();
            timer = phasetimer;
        }
    }
    private void ChangeScale()
    {
        transform.localScale = new Vector3(transform.localScale.x + expanderSize, transform.localScale.y + expanderSize, 0.5f);
        phase++;
    }
}
