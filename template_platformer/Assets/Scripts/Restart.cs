﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    private string sceneName;
    private void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName= currentScene.name;
    }
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
            switch (sceneName)
            {
                case "Plataformer Scene":
                    SceneManager.LoadScene(0);
                    break;
                case "Lvl1":
                    SceneManager.LoadScene(1);
                    break;
                case "Lvl2":
                    SceneManager.LoadScene(2);
                    break;
                case "Lvl3":
                    SceneManager.LoadScene(3);
                    break;
                default:
                    SceneManager.LoadScene(0);
                    break;
            }
            
        }
    }
}
