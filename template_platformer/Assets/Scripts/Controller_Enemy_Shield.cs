using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy_Shield : MonoBehaviour
{
    MeshRenderer render;
    Collider col;
    [SerializeField] float shieldTimer;
    [SerializeField] float timer = 0;
    [SerializeField] bool shield;
    [SerializeField] int forceRepeler;
    void Start()
    {
        shield = false;
        render= GetComponent<MeshRenderer>();
        col= GetComponent<Collider>();
        render.enabled = false;
        col.enabled = false;
        
    }
    void Update()
    {
        Timer();
        if(shield)
        {
            ShowShield();
        }
        else
        {
            HideShield();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Rigidbody rbCollision = collision.gameObject.GetComponent<Rigidbody>();
            rbCollision.AddForce(-transform.position * forceRepeler, ForceMode.Impulse);
        }
    }
    private void Timer()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            shield = !shield;
            timer = shieldTimer;
        }
    }
    private void ShowShield()
    {
        render.enabled = true;
        col.enabled = true;
    }
    private void HideShield()
    {
        render.enabled = false;
        col.enabled = false;
    }
}
