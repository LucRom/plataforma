using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Player_Expander : MonoBehaviour
{
    [SerializeField] Vector3 baseScale;
    [SerializeField] float limite;
    public Text skill;
    Rigidbody rb;
    Controller_Player player;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        limite = 4f;
        baseScale = new Vector3(1,1,0.5f);
        player = GetComponent<Controller_Player>();
    }
    void Update()
    {
        if(GameManager.actualPlayer == player.playerNumber)
        {
            Controller_Player.hideskilltext = false;
            skill.text = "Ctrl + arrows to expand";
            if (Input.GetKeyDown(KeyCode.F))
            {
                transform.localScale = baseScale;
                rb.mass = 1;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.RightArrow) && transform.localScale.x <= limite)
            {
                transform.localScale = new Vector3(transform.localScale.x + 0.5f, transform.localScale.y, 0.5f);
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.LeftArrow) && transform.localScale.x >= 1)
            {
                transform.localScale = new Vector3(transform.localScale.x - 0.5f, transform.localScale.y, 0.5f);
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.UpArrow) && transform.localScale.y <= limite)
            {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y + 0.5f, 0.5f);
                rb.mass += 1;
                if (rb.mass >= 4)
                {
                    rb.mass = 4;
                }
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.DownArrow) && transform.localScale.y >= 1)
            {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y - 0.5f, 0.5f);
                rb.mass -= 1;
                if (rb.mass < 1)
                {
                    rb.mass = 1;
                }
            }
        }
    }
}
