using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Area : MonoBehaviour
{
    [SerializeField] public static float damage = 0;
    void Start()
    {
        
    }
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy") && Controller_Player_Smasher.charging == false)
        {
            if(other.gameObject.name == "Enemy_Controller")
            {
                other.gameObject.GetComponent<Controller_Enemy_Controller>().BackToNormal();
            }
            other.gameObject.GetComponent<Controller_Health_Enemy>().DamageToEnemy(damage);
        }
    }
}
