using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Player_Smasher : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] GameObject area;
    [SerializeField] float jumpForce;
    [SerializeField] float jumpMultiplier;
    [SerializeField] bool canJump;
    public static bool charging;
    Controller_Player player;
    public Text skill;
    void Start()
    {
        charging = false;
        canJump = true;
        jumpForce = 10;
        rb = GetComponent<Rigidbody>();
        player= GetComponent<Controller_Player>();
    }

    void Update()
    {
        if(GameManager.actualPlayer == player.playerNumber)
        {
            Controller_Player.hideskilltext = false;
            skill.text = "S to charge jump";
            if (Input.GetKeyDown(KeyCode.W) && canJump)
            {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            }
            if(Input.GetKey(KeyCode.S) && transform.localScale.y > 1.2f && canJump)
            {
                charging = true;
                transform.localScale = new Vector3(2.5f, transform.localScale.y - Time.deltaTime, 0.5f);
                area.transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime, 0.1f, 0.5f);
                jumpForce += Time.deltaTime *jumpMultiplier;
                Controller_Area.damage += Time.deltaTime * jumpMultiplier;
            }
            if(Input.GetKeyUp(KeyCode.S) && canJump)
            {
                charging = false;
                transform.localScale = new Vector3(2.5f, 2.5f, 0.5f);
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                jumpForce = 10;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            area.transform.localScale = new Vector3(1.5f, 0.1f, 2);
            canJump = true;
            Controller_Area.damage = 0;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            canJump= false;
        }
    }
}
