using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField] GameObject menuStart;
    [SerializeField] GameObject menuLevel;

    void Start()
    {
        
    }

    public void showStart() 
    {
        menuLevel.SetActive(false);
        menuStart.SetActive(true);
    }

   public void showLevels() 
    {
        menuLevel.SetActive(true);
        menuStart.SetActive(false);  
    }

    public void loadLevel1() 
    {
        SceneManager.LoadScene(1);
    }
    public void loadLevel2()
    {
        SceneManager.LoadScene(2);
    }
    public void loadLevel3()
    {
        SceneManager.LoadScene(3);
    }

}
