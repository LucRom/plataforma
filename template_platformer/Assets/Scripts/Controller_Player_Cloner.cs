using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Controller_Player_Cloner : MonoBehaviour
{
    [SerializeField] GameObject clone;
    [SerializeField] int maxClone;
    [SerializeField] int cloneCount;
    [SerializeField] float cloneLifeTime;
    [SerializeField] float timer;
    [SerializeField] GameObject tempclone;
    Controller_Player player;
    public Text skill;
    void Start()
    {
        player = GetComponent<Controller_Player>();
        cloneCount = 0;
        timer = cloneLifeTime;
    }
    void Update()
    {
        if(GameManager.actualPlayer == player.playerNumber)     
        {
            Controller_Player.hideskilltext = false;
            skill.text = "F to clone";
            if (Input.GetKeyDown(KeyCode.F) && cloneCount < maxClone)
            {
                cloneCount++;
                timer = cloneLifeTime;
                tempclone = Instantiate(clone, transform.position, Quaternion.identity);
            }
            timer -= Time.deltaTime;
        }
        if (timer <= 0 && cloneCount >= maxClone)
        {
            cloneCount--;
            Destroy(tempclone);
            timer = cloneLifeTime;
        }
    }
}
