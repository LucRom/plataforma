using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Controller_Player_Dust : MonoBehaviour
{
    [SerializeField] bool dustMode;
    [SerializeField] MeshRenderer mesh;
    [SerializeField] GameObject dustParticle;
    Controller_Player player;
    public Text skill;
    void Start()
    {
        dustParticle.SetActive(false);
        mesh= GetComponent<MeshRenderer>();
        dustMode = false;
        player = GetComponent<Controller_Player>();
    }
    void Update()
    {
        if(GameManager.actualPlayer == player.playerNumber)
        {
            Controller_Player.hideskilltext = false;
            skill.text = "F to turn dust";
            if (Input.GetKeyDown(KeyCode.F))
            {
                dustMode = !dustMode;
            }
            if (dustMode)
            {
                mesh.enabled = false;
                dustParticle.SetActive(true);
                gameObject.layer = 6;
            }
            else
            {
                mesh.enabled = true;
                dustParticle.SetActive(false);
                gameObject.layer = 10;
            }
        }

    }
}
