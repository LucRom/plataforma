using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Controller_Player_Bird : MonoBehaviour
{
    [SerializeField] float jumpforce;
    [SerializeField] bool attach;
    [SerializeField] bool attechedToPlayer;
    [SerializeField] bool realease;
    GameObject target;
    Rigidbody rb;
    RaycastHit hit;
    Controller_Player player;
    public Text skill;
    void Start()
    {
        realease= false;
        rb = GetComponent<Rigidbody>();
        attach = false;
        player = GetComponent<Controller_Player>();
    }
    void Update()
    {
        if (GameManager.actualPlayer == player.playerNumber)
        {
            Controller_Player.hideskilltext = false;
            skill.text = "F to grab a player";
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(Vector2.up * jumpforce, ForceMode.Impulse);
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                attach = !attach;
            }
            if (attach)
            {
                gameObject.tag = "Untagged";
            }
            if (attechedToPlayer)
            {
                target.transform.position = new Vector3(transform.position.x, transform.position.y - 2, 0);
            }
            if(attach == false && attechedToPlayer)
            {
                attechedToPlayer = false;
                realease = true;
            }
            if(target != null && realease == true)
            {
                gameObject.tag = "Player";
                target.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                target.transform.parent = null;
                target = null;
                realease = false;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && attach && target == null)
        {
            Physics.Raycast(transform.position, Vector3.down, out hit, 0.5f);
            target= GameObject.Find(hit.transform.name);
            target.transform.parent = transform;
            attechedToPlayer = true;
        }
    }
}
