using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy_Controller : MonoBehaviour
{
    public List<GameObject> objectsToControl= new List<GameObject>();
    [SerializeField] float timer;
    [SerializeField] float objectTimer;
    [SerializeField] Material mat_Red;
    [SerializeField] Material mat_Gray;
    Controller_Health_Enemy health;
    private int randomObject =0;
    void Start()
    {
        timer = objectTimer;
        health = GetComponent<Controller_Health_Enemy>();
    }
    void Update()
    {
        Timer();

    }
    private void Timer()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            EnableObject();
            int rndNumber = UnityEngine.Random.Range(1, 3);
            randomObject = UnityEngine.Random.Range(0, objectsToControl.Count);
            if (rndNumber == 1)
            {
                RotateObject();
            }
            else
            {
                DisableObject();
            }
            timer = objectTimer;
        }
    }
    private void RotateObject()
    {
        objectsToControl[randomObject].transform.rotation = Quaternion.Euler(0,0, UnityEngine.Random.Range(1, 90));
    }
    private void DisableObject()
    {
        objectsToControl[randomObject].GetComponent<MeshRenderer>().enabled = false;
        objectsToControl[randomObject].GetComponent<BoxCollider>().enabled = false;
    }
    private void EnableObject()
    {
        objectsToControl[randomObject].GetComponent<MeshRenderer>().enabled = true;
        objectsToControl[randomObject].GetComponent<BoxCollider>().enabled = true;
    }
    public void BackToNormal()
    {
        foreach (GameObject obj in objectsToControl)
        {
            obj.GetComponent<MeshRenderer>().enabled = true;
            obj.GetComponent<BoxCollider>().enabled = true;
            obj.transform.rotation = Quaternion.Euler(0, 0, 0);
            obj.GetComponent<Renderer>().materials[1].shader = null;
        }

    }
}
