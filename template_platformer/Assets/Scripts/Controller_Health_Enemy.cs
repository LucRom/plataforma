using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Health_Enemy : MonoBehaviour
{
    [SerializeField]public float enemyHealth = 10;
    void Start()
    {
        
    }
    void Update()
    {
        
    }
    public void DamageToEnemy(float damage)
    {
        enemyHealth -= damage;
        if(enemyHealth < 0)
        {
            Destroy(gameObject);
        }
    }
}
