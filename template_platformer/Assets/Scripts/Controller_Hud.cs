﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Controller_Hud : MonoBehaviour
{
    public Text gameOverText;
    public Text skill;


    void Start()
    {
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Controller_Player.hideskilltext)
        {
            skill.text = "";
        }
        if (GameManager.gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over" ;
            gameOverText.gameObject.SetActive(true);
        }
        if (GameManager.winCondition)
        {
            Time.timeScale = 0;
            gameOverText.text = "You win. Press M to go back.";
            gameOverText.gameObject.SetActive(true);
        }
    }
}
