using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Button : MonoBehaviour
{
    public GameObject target;
    public Material greenClone;
    void Start()
    {
        target.SetActive(false);
    }

    void Update()
    {
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Clone"))
        {
            target.SetActive(true);
            //target.transform.position = new Vector3(1.3f,6.2f,0);
            gameObject.GetComponent<Renderer>().material = greenClone;
        }
    }
}
