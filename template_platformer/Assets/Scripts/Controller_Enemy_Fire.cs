using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.ParticleSystemJobs;

public class Controller_Enemy_Fire : MonoBehaviour
{

    [SerializeField] GameObject fireGO;
    [SerializeField] float timer;
    [SerializeField] float fireCooldown = 0;
    [SerializeField] float fireLifeTime = 10;
    bool fireEnabled;
    //int fireStatus = 0;

    void Start()
    {
        timer = UnityEngine.Random.Range(5, 10);
        GenerateFire();
    }

    void GenerateFire()
    {
        fireLifeTime = 10f;
        fireCooldown = 8;
        fireGO.SetActive(true);
        fireEnabled = true;
    }

    void TurnOffFire() 
    {
        fireGO.SetActive(false);
        fireEnabled = false;
    }

    void Update()
    {
        Timer();
    }

    private void Timer()
    {
        timer -= Time.deltaTime;
   
        if (timer <= 0)
        { 
            timer = UnityEngine.Random.Range(5, 10);
            if (fireCooldown <= 0)
            {
                GenerateFire();
            }
        }

        if (fireEnabled == true)
        {
            fireLifeTime -= Time.deltaTime;

            if (fireLifeTime <= 0)
            {
                TurnOffFire();
                
            }
        }
        else if (fireEnabled == false)
        { 
            fireCooldown -= Time.deltaTime;
        }
    }

}
