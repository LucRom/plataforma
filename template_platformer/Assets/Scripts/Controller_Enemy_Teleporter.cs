using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

public class Controller_Enemy_Teleporter : MonoBehaviour
{
    public List<GameObject> tpZone = new List<GameObject>();
    [SerializeField] float forceSpeed;
    [SerializeField] float timer;
    [SerializeField] bool goRight;
    Rigidbody rb;
    void Start()
    {
        goRight= true;
        rb= GetComponent<Rigidbody>();
        timer = UnityEngine.Random.Range(1, 6);
    }
    void Update()
    {
        Timer();
        if(goRight)
        {
            GoRight();
        }
        else
        {
            GoLeft();
        }
    }
    private void GoLeft()
    {
        rb.AddForce(Vector3.left * forceSpeed, ForceMode.Force);
    }
    private void GoRight()
    {
        rb.AddForce(Vector3.right * forceSpeed, ForceMode.Force);
    }
    private void Timer()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            goRight = !goRight;
            timer = UnityEngine.Random.Range(1, 6);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            int zonaRandom = UnityEngine.Random.Range(1, tpZone.Count);
            collision.gameObject.transform.position = tpZone[zonaRandom].transform.position;
        }
    }
}
